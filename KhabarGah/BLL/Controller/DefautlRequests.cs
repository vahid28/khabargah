﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KhabarGah.DAL;
using System.Text;

namespace KhabarGah.BLL.Controller
{
    public class DefautlRequests
    {
        public string NewsAgencyAndCategories()
        {
            NewsDAL myNews = new NewsDAL();
            string[] NewsAgencies = myNews.getAllNewsAgencies();
            string[] NewsCategories = myNews.getAllNewsCategories();
            StringBuilder myStructure = new StringBuilder();
            myStructure.Append("<div id='NewsSelection'>");
            myStructure.Append("<form action=''>");
            myStructure.Append("<div id='NewsAgencySelection'>");
            myStructure.Append("<ul>");
            for (int counter = 0; counter < NewsAgencies.Length; counter++)
            {
                myStructure.Append("<li>");
                myStructure.Append("<span>");
                myStructure.Append("<a href='/" + NewsAgencies[counter].Replace(' ', '-') + "' />" + NewsAgencies[counter] + "</a>");
                myStructure.Append("<input type='checkbox' name='NewsAgency' id='checkboxG1' class='css-checkbox' value='" + NewsAgencies[counter] + "' />");

                myStructure.Append("</span>");
                myStructure.Append("</li>");
            }
            myStructure.Append("</ul>");
            myStructure.Append("</div>");
            myStructure.Append("<div id='NewsCategorySelection'>");
            myStructure.Append("<ul>");
            for (int counter = 0; counter < NewsCategories.Length; counter++)
            {
                myStructure.Append("<li>");
                myStructure.Append("<span>");
                myStructure.Append("<a href='/" + NewsCategories[counter].Replace(' ', '-') + "' />" + NewsCategories[counter] + "</a>");
                myStructure.Append("<input type='checkbox' name='NewsCategory' id='checkboxG1' class='css-checkbox' value='" + NewsCategories[counter] + "' />");

                myStructure.Append("</span>");
                myStructure.Append("</li>");
            }
            myStructure.Append("</ul>");
            myStructure.Append("</div>");
            myStructure.Append("<input type='submit' value='ارسال' class='myButton'/> ");
            myStructure.Append("</form>");
            myStructure.Append("</div>");
            return myStructure.ToString();
        }
        public string lastNews()
        {
            NewsDAL myNews = new NewsDAL();
            return myNews.gettingNews();
        }
    }
}