﻿using KhabarGah.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using HAP = HtmlAgilityPack;

namespace KhabarGah.BLL.DLL
{
    public class Crawler
    {
        public HAP.HtmlNodeCollection getRequestAndParse(string url, string path)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
            {
                var parser = new HAP.HtmlDocument();
                parser.Load(data);
                var root = parser.DocumentNode;
                var myLinks = root.SelectNodes(path);
                return myLinks;
            }
        }
        public void getRequestAndParse(string NewsAgency, string url, string pathOfTitle, string pathOftext, string pathOfImage, string NewsCategory)
        {
            NewsDAL DataAccess = new NewsDAL(NewsAgency, NewsCategory);
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
            {
                var parser = new HAP.HtmlDocument();
                parser.Load(data);
                var root = parser.DocumentNode;
                var myLinks = root.SelectNodes(pathOfTitle);
                //var myLinks1 = root.SelectNodes(pathOftext);
                var myLinks2 = root.SelectNodes(pathOfImage);
                var myLinks1 = root.SelectNodes("//html");
                string title2 = "";
                StringBuilder text = new StringBuilder();
                if (myLinks != null)
                {
                    foreach (var title1 in myLinks)
                    {
                        title2 = title1.InnerHtml;
                    }
                    foreach (var text1 in myLinks1)
                    {
                        text.Append(text1.InnerHtml);
                    }
                    DateTime now = DateTime.Now;
                    DataAccess.AddNews(title2.Trim(), text.ToString(), url.Trim(), now);
                }
                using (WebClient Client = new WebClient())
                {
                    Client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    Client.Encoding = Encoding.UTF8;
                    StringBuilder mysrc = new StringBuilder();
                    if (myLinks2 != null)
                    {
                        string title = "";
                        foreach (var title1 in myLinks)
                        {
                            title = title1.InnerHtml;
                        }
                        mysrc.Append(myLinks2.ToArray()[0].GetAttributeValue("src", ""));
                        int number = mysrc.Length;
                        mysrc.Remove(number - 2, 2);
                        string my = mysrc.ToString();
                        //string InNewsCategory = Convert.ToString();
                        //string NewsCategory = Convert.ToString(NewsCategory);
                        var a = DataAccess.NewCategoryId(NewsCategory.Trim());
                        if (!System.IO.Directory.Exists(@"D:\work\KhabarGah\KhabarGah\Data\isna\" + a))
                            System.IO.Directory.CreateDirectory(@"D:\work\KhabarGah\KhabarGah\Data\isna\" + a);
                        var b = DataAccess.IdOfNews(title.Trim());
                        string fileName = @"D:\work\KhabarGah\KhabarGah\Data\isna\" + a + "\\" + b + ".jpg";
                        try
                        {
                            Client.DownloadFile(my, fileName);
                        }
                        catch
                        {

                        }
                        my += "/4";
                        fileName = @"D:\work\KhabarGah\KhabarGah\Data\isna\" + a + "\\" + b + "-2.jpg";

                        using (WebClient Client1 = new WebClient())
                        {
                            Client1.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                            try
                            {
                                Client1.DownloadFile(my, fileName);
                            }
                            catch
                            { }
                        }
                    }
                }
            }
        }
        public bool checkNews(string title, string link, string NA, string NC)
        {
            NewsDAL NDAL = new NewsDAL(NA, NC);
            return NDAL.checkNews(title, link);
        }
    }
}