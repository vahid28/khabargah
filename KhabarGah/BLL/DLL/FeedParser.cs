﻿using KhabarGah.DAL;
using System;
using System.Collections.Generic;
using System.IO;
//using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using HAP = HtmlAgilityPack;

namespace KhabarGah.BLL.DLL
{
    public partial class FeedParser
    {
        public string[] parseXml(string xmlString, string NewsAgency, string newsCategory)
        {
            var parser = new HAP.HtmlDocument();
            NewsDAL myNews = new NewsDAL(NewsAgency.Trim().Replace('ي', 'ی'), newsCategory.Trim().Replace('ي', 'ی'));
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(xmlString);
            XmlNodeList list = xdoc.DocumentElement.SelectNodes("//item");
            var count = list.Count;
            for (int counter = 0; counter < 2; counter++)
            {
                StringBuilder myDescription = new StringBuilder();
                StringBuilder myPubDate = new StringBuilder();
                XmlNode title = list.Item(counter).SelectSingleNode("title");
                XmlNode description = list.Item(counter).SelectSingleNode("description");
                XmlNode link = list.Item(counter).SelectSingleNode("link");
                XmlNode pubDate = list.Item(counter).SelectSingleNode("pubDate");
                if (pubDate == null)
                    myPubDate.Append("");
                else
                    myPubDate.Append(pubDate.InnerXml.Trim());
                if (description == null)
                    myDescription.Append("");
                else
                    myDescription.Append(description.InnerXml.Trim());
                bool flag = myNews.checkNews(title.InnerXml.Trim(), link.InnerXml.Trim());
                if (flag)
                {
                    string imageUrl = "";
                    if (NewsAgency == "الف")
                    {
                        imageUrl = getBetween(description.InnerXml, "src=", ".jpg");
                        imageUrl = getBetween(imageUrl, "http:", ".jpg");
                    }
                    if (NewsAgency == "افکارنیوز")
                    {
                        imageUrl = getBetween(description.InnerXml, "src=", ".jpg");
                        imageUrl = getBetween(imageUrl, "http:", ".jpg");
                    }
                    HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(link.InnerXml.Trim());
                    myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                    try
                    {
                        HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                        using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                        {
                            StringBuilder text = new StringBuilder();
                            parser.Load(data);
                            var root = parser.DocumentNode;
                            var myLinks1 = root.SelectNodes("//html");
                            foreach (var text1 in myLinks1)
                            {
                                text.Append(text1.InnerHtml);
                            }
                            HAP.HtmlNodeCollection myLinks2 = null;
                            if (NewsAgency == "فارس")
                                myLinks2 = root.SelectNodes("//span[@id='ctl00_bodyHolder_newstextDetail_nwstxtPicPane']/img");
                            if (NewsAgency == "تابناک")
                                myLinks2 = root.SelectNodes("//div[@style='width: 570px;padding-top: 15px;margin-bottom: 15px;text-align: center;border: 1px solid #E0E0E0']/a/img");
                            if (NewsAgency == "خبرآنلاین")
                                myLinks2 = root.SelectNodes("//div[@class='newsPhoto']/img");
                            if (NewsAgency == "ایرنا")
                                myLinks2 = root.SelectNodes("//div[@id='ctl00_ctl00_ContentPlaceHolder_ContentPlaceHolder_NewsContent3_bodytext']/img");
                            if (NewsAgency == "مهر")
                                myLinks2 = root.SelectNodes("//div[@class='detailImageHolder']/a/img");
                            if (NewsAgency == "مشرق")
                                myLinks2 = root.SelectNodes("//div[@class='body']/img");
                            if (NewsAgency == "دولت")
                                myLinks2 = root.SelectNodes("//div[@class='NewsDetailPic NewsHidden   flL']/a/img");
                            if (NewsAgency == "پلیس فتا")
                                myLinks2 = root.SelectNodes("//div[@class='field field-type-filefield field-field-ct-news-pic']/img");
                            if (NewsAgency == "تسنیم")
                                myLinks2 = root.SelectNodes("//article[@id='k2Container']/img");
                            try
                            {
                                foreach (var inMyLinks2 in myLinks2)
                                {
                                    imageUrl = inMyLinks2.GetAttributeValue("src", "");
                                }
                            }
                            catch (Exception)
                            {

                            }
                            //if (imageUrl != "")
                            //{
                            //    if (NewsAgency == "فارس")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "fars");
                            //    if (NewsAgency == "تابناک")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "tabnak");
                            //    if (NewsAgency == "الف")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "alef");
                            //    if (NewsAgency == "خبرآنلاین")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "khabarOnline");
                            //    if (NewsAgency == "ایرنا")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "irna");
                            //    if (NewsAgency == "مهر")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "mehr");
                            //    if (NewsAgency == "مشرق")
                            //        getImage("http://www.mashreghnews.ir/"+imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "mashregh");
                            //    if (NewsAgency == "دولت")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "dolat");
                            //    if (NewsAgency == "پلیس فتا")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "policeFata");
                            //    if (NewsAgency == "تسنیم")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "tasnim");
                            //    if (NewsAgency == "افکارنیوز")
                            //        getImage(imageUrl, title.InnerXml.Trim(), NewsAgency, newsCategory, "afkarNews");
                            //}
                            if (imageUrl != "")
                            {
                                myNews.AddNews(title.InnerXml.Trim(), text.ToString().Trim(), link.InnerXml.Trim(), DateTime.Now, myPubDate.ToString().Trim(), myDescription.ToString().Trim(), imageUrl);
                            }
                            else
                            {
                                myNews.AddNews(title.InnerXml.Trim(), text.ToString().Trim(), link.InnerXml.Trim(), DateTime.Now, myPubDate.ToString().Trim(), myDescription.ToString().Trim(), "");
                            }
                        }
                    }
                    catch (Exception)
                    { }
                }
            }

            return null;
        }
        public void getImage(string url, string title, string NA, string NC, string dir)
        {
            NewsDAL NDAL = new NewsDAL(NA.Trim().Replace('ي', 'ی'), NC.Trim().Replace('ي', 'ی'));
            var a = NDAL.NewCategoryId(NC);
            var b = NDAL.IdOfNews(title);
            string fileName = @"D:\work\KhabarGah\KhabarGah\Data\" + dir + "\\" + a + "\\" + b + ".jpg";

            if (!System.IO.Directory.Exists(@"D:\work\KhabarGah\KhabarGah\Data\" + dir + "\\" + a.ToString()))
                System.IO.Directory.CreateDirectory(@"D:\work\KhabarGah\KhabarGah\Data\" + dir + "\\" + a.ToString());
            using (WebClient Client1 = new WebClient())
            {
                Client1.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                try
                {
                    Client1.DownloadFile(url, fileName);
                }
                catch
                { }
            }
        }
        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                string vahid = "";
                vahid += strStart;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, 0) + strEnd.Length;
                int y = End - Start;
                for (int counter = 0; counter < y; counter++)
                {
                    vahid += strSource[Start];
                    Start++;
                }
                return vahid;
            }
            else
            {
                return "";
            }
        }
    }
}