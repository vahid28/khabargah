﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HAP = HtmlAgilityPack;
using KhabarGah.DAL;

namespace KhabarGah.BLL.DLL
{
    public partial class FeedParser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NewsDAL v = new NewsDAL();
            //v.AddNews(null,null,null,DateTime.Now);
            Crawler myCrowler = new Crawler();
            var categories = myCrowler.getRequestAndParse("http://farsnews.com/rsslinks.php", "//div[@class='centercolumn']/div/a");
            try
            {
                //foreach (var inCategory in categories)
                //{
                //    if (inCategory.InnerHtml == "اخبار برگزيده")
                //    {
                //        continue;
                //    }
                //    FeedParser feedParserInstance = new FeedParser();
                //    HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://farsnews.com" + inCategory.GetAttributeValue("href", "").ToString());
                //    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                //    using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                //    {
                //        string page = data.ReadToEnd();
                //        feedParserInstance.parseXml(page, "فارس", inCategory.InnerHtml);
                //    }
                //}
                procc("http://farsnews.com/rss.php", "اخبار برگزيده", "فارس");
                procc("http://farsnews.com/rss.php?srv=1", "سياسي", "فارس");
                procc("http://farsnews.com/rss.php?srv=2", "اقتصادي", "فارس");
                procc("http://farsnews.com/rss.php?srv=3", "اجتماعي", "فارس");
                procc("http://farsnews.com/rss.php?srv=4", "ورزشي", "فارس");
                procc("http://farsnews.com/rss.php?srv=6", "بين الملل", "فارس");
                procc("http://farsnews.com/rss.php?srv=7", "فرهنگي", "فارس");
                procc("http://farsnews.com/rss.php?srv=8", "عکس", "فارس");
                procc("http://farsnews.com/rss.php?srv=11", "ديدگاه", "فارس");
                procc("http://farsnews.com/rss.php?srv=15", "استانها", "فارس");
                procc("http://farsnews.com/rss.php?srv=21", "گفتگو", "فارس");
                procc("http://farsnews.com/rss.php?srv=30", "فضاي مجازي", "ایرنا");
                procc("http://farsnews.com/rss.php?srv=35", "دیپلماسی عمومی و جنگ نرم", "فارس");
            }
            catch (Exception E)
            {
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////Tabnak
            categories = myCrowler.getRequestAndParse("http://www.tabnak.ir/fa/rss", "//div[@class='rss_lists_container']/div[@class='rss_block']/div[@class='rss_row']/a");
            try
            {
                int counter = 0;
                foreach (var inCategory in categories)
                {
                    counter++;
                    if (counter == 1 || counter == 2)
                    {
                        continue;
                    }
                    if (counter == 10)
                    {
                        break;
                    }
                    FeedParser feedParserInstance = new FeedParser();
                    HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://www.tabnak.ir" + inCategory.GetAttributeValue("href", "").ToString());
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                    {
                        string page = data.ReadToEnd();
                        switch (inCategory.InnerHtml)
                        {
                            case "http://tabnak.ir/fa/rss/3":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "اجتماعی");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/6":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "اقتصادی");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/8":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "عکس");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/5":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "مجلس");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/2":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "ورزشی");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/4":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "وعده ها");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/9":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "ویژه نامه");
                                    break;
                                }
                            case "http://tabnak.ir/fa/rss/11":
                                {
                                    feedParserInstance.parseXml(page, "تابناک", "عاسف");
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception)
            { }
            ///////////////////////////////////////////////////////////////////////////////////Alef
            try
            {
                FeedParser feedParserInstance = new FeedParser();
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssci.uy2-s2tuu-5b1,ydk.8al2as.2.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "سیاسی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssew.skj1zjyss1rhx2k4m.ib9jbx.j.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "اقتصادی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssf0.47wrywq44r,6mp73f.aigwij.w.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "اجتماعی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssdx.gmyefy,ggeltshmci.62ay2x.y.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "فرهنگ و هنر");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssew.skj1zjyss1rhx2k4m.jbj.z9eib.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "کتاب");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://alef.ir/rssb5.-er48r6--4qhfle2m.rur.8i8pu.xml");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "الف", "گزارش تصویری");
                }
            }
            catch (Exception)
            { }
            ////////////////////////////////////////////////////////////////////////////////////khabarOnline
            try
            {
                FeedParser feedParserInstance = new FeedParser();
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/politics");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "سیاسی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/economy");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "اقتصادی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/culture");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "فرهنگی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/society");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "جامعه");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/World");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "بین الملل");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/sport");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "ورزشی");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/science");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "دانش");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/ict");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "فناوری اطلاعات");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/comic");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "طنز و کاریکاتور");
                }
                myRequest = (HttpWebRequest)WebRequest.Create("http://khabaronline.ir/rss/service/weblog");
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
                myResponse = (HttpWebResponse)myRequest.GetResponse();
                using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
                {
                    string page = data.ReadToEnd();
                    feedParserInstance.parseXml(page, "خبرآنلاین", "وبلاگ");
                }
            }
            catch (Exception)
            {

            }
            ////////////////////////////////////////////////////////////////////////////////////////////irna
            try
            {
                procc("http://www.irna.ir/fa/rss.aspx?kind=5", "سیاسی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=20", "اقتصادی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=32", "اجتماعی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=41", "فرهنگی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=180", "علمی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=14", "ورزشی", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=1", "بین الملل", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=54", "استانها", "ایرنا");

                procc("http://www.irna.ir/fa/rss.aspx?kind=51", "عکس", "ایرنا");


            }
            catch (Exception)
            {

            }
            /////////////////////////////////////////////////////////////////////////////////////////////mehr
            try
            {
                procc("http://www.mehrnews.com/rss/service/7", "سیاسی", "مهر");

                procc("http://www.mehrnews.com/rss/service/25", "اقتصادی", "مهر");

                procc("http://www.mehrnews.com/rss/service/6", "اجتماعی", "مهر");

                procc("http://www.mehrnews.com/rss/service/1", "فرهنگ و هنر", "مهر");

                procc("http://www.mehrnews.com/rss/service/2", "فرهنگ و ادب", "مهر");

                procc("http://www.mehrnews.com/rss/service/3", "دین و اندیشه", "مهر");

                procc("http://www.mehrnews.com/rss/service/4", "حوزه و دانشگاه", "مهر");

                procc("http://www.mehrnews.com/rss/service/5", "دانش و فناوری", "مهر");

                procc("http://www.mehrnews.com/rss/service/8", "بین الملل", "مهر");

                procc("http://www.mehrnews.com/rss/service/9", "ورزشی", "مهر");

                procc("http://www.mehrnews.com/rss/service/11", "عکس", "مهر");

                procc("http://www.mehrnews.com/rss/service/19", "استانها", "مهر");


            }
            catch (Exception)
            {

            }
            //////////////////////////////////////////////////////////////////////////////yjc
            try
            {
                procc("http://yjc.ir/fa/rss/3", "سیاسی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/6", "اقتصادی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/5", "اجتماعی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/4", "فرهنگی هنری", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/7", "علمی پزشکی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/8", "ورزشی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/9", "بین‌الملل", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/10", "وب‌گردی", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/11", "چند رسانه‌ای", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/2", "عکس", "باشگاه خبرنگاران");

                procc("http://yjc.ir/fa/rss/12", "استانها", "باشگاه خبرنگاران");


            }
            catch (Exception)
            {

            }
            ////////////////////////////////////////////////////////////////////////////////////mashregh
            try
            {
                procc("http://mashreghnews.ir/fa/rss/2", "سیاسی", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/16", "اقتصادی", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/14", "اجتماعی", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/4", "فرهنگی", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/8", "چندرسانه‌ای", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/10", "ورزشی", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/5", "بین‌الملل", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/7", "جنگ نرم", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/3", "گزارش‌ویژه", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/9", "جهاد و مقاومت", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/36", "تاریخ", "مشرق");

                procc("http://mashreghnews.ir/fa/rss/11", "دفاعی", "مشرق");


            }
            catch (Exception)
            {

            }
            //////////////////////////////////////////////////////////////////// dolat
            try
            {
                procc("http://www.dolat.ir/RSS/", "همه", "دولت");
            }
            catch (Exception)
            {

            }
            ///////////////////////////////////////////////////////////////////////// police fata
            //////////////try
            //////////////{
            //////////////    procc("http://www.cyberpolice.ir/atom.xml", "همه", "پلیس فتا");
            //////////////}
            //////////////catch (Exception)
            //////////////{

            //////////////}
            //////////////////////////////////////////////////////////////////// tasnim
            try
            {
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=1", "سیاسی", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=8", "بین الملل", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=1115", "بیداری اسلامی", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=4", "فرهنگی", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=7", "اقتصادی", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=3", "ورزشی", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=2", "اجتماعی", "تسنیم");
                //procc("http://multimedia.tasnimnews.com/rss/feed/?d=1&c=8&m=7&alt=%DA%AF%D8%B2%D8%A7%D8%B1%D8%B4%20%D9%87%D8%A7%DB%8C%20%D8%AA%D8%B5%D9%88%DB%8C%D8%B1%DB%8C", "عکس", "تسنیم");
                //procc("http://multimedia.tasnimnews.com/Graphic//rss/feed/?d=1&c=10&m=7&alt=%DA%AF%D8%B1%D8%A7%D9%81%DB%8C%DA%A9%20%D9%88%20%DA%A9%D8%A7%D8%B1%DB%8C%DA%A9%D8%A7%D8%AA%D9%88%D8%B1", "گرافیک و کاریکاتور", "تسنیم");
                //procc("http://multimedia.tasnimnews.com/rss/feed/?d=1&c=9&m=7&alt=%D8%B5%D9%88%D8%AA%20%D9%88%20%D9%81%DB%8C%D9%84%D9%85", "صوت و فیلم", "تسنیم");
                procc("http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=6", "استانها", "تسنیم");
            }
            catch (Exception)
            {

            }
            try
            {
                procc("http://www.afkarnews.ir/rssci.uy2-s2tuu-5b1,ydk.8al2as.2.xml", "سیاسی", "افکارنیوز");
                procc("http://www.afkarnews.ir/rssgx.dja5qa6dd51kwvjoe.4rparn.a.xml", "بین الملل", "افکارنیوز");
                procc("http://www.afkarnews.ir/rssau.dg4t64qddth9e,gc7.1k54ko.4.xml", "مذهبی", "افکارنیوز");
                procc("http://www.afkarnews.ir/rssdx.gmyefy,ggeltshmci.62ay2x.y.xml", "فرهنگ و هنر", "افکارنیوز");
                procc("http://www.afkarnews.ir/rssew.skj1zjyss1rhx2k4m.ib9jbx.j.xml", "اقتصادی", "افکارنیوز");
                procc("http://www.afkarnews.ir/rsshi.c120z29cc0y3xu1go.dtf2t-.2.xml", "ورزشی", "افکارنیوز");
                procc("http://www.afkarnews.ir/rssf0.47wrywq44r,6mp73f.aigwij.w.xml", "اجتماعی", "افکارنیوز");
            }
            catch (Exception)
            {

            }
        }
        public void procc(string link, string category, string agency)
        {
            FeedParser feedParserInstance = new FeedParser();
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(link);
            myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            using (StreamReader data = new StreamReader(myResponse.GetResponseStream()))
            {
                string page = data.ReadToEnd();
                feedParserInstance.parseXml(page, agency, category);
            }
        }
    }
}