﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KhabarGah.BLL.DLL
{
    public partial class crowler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Crawler myCrowler = new Crawler();
            var myNodes = myCrowler.getRequestAndParse("http://www.isna.ir/", "//div[@class='navbar col']/ul/li/div/a");
            try
            {
                foreach (var myLinks in myNodes)
                {
                    if (myLinks.InnerHtml != "صفحه اصلی" && myLinks.GetAttributeValue("class", "").ToString() != "feed" && myLinks.InnerHtml != "عناوین کل اخبار")
                    {
                        var myNodes1 = myCrowler.getRequestAndParse("http://www.isna.ir" + myLinks.GetAttributeValue("href", "").ToString(), "//div[@class='item-list latest-news']/div/span/a");
                        try
                        {
                            foreach (var myLinks1 in myNodes1)
                            {
                                if (myLinks1.GetAttributeValue("href", "").ToString() != null)
                                {
                                    bool flag;
                                    flag = myCrowler.checkNews(myLinks1.InnerHtml.Trim(), "http://www.isna.ir" + myLinks1.GetAttributeValue("href", "").ToString().Trim(), "ایسنا", myLinks.InnerHtml.Trim());
                                    if (flag)
                                    {
                                        myCrowler.getRequestAndParse("ایسنا", "http://www.isna.ir" + myLinks1.GetAttributeValue("href", "").ToString(), "//div[@class='titrpart']/div[@class='titr']/h1/p", "//div[@class='news']/div[@class='body']/p", "//div[@class='main-image']/a/img", myLinks.InnerHtml);
                                    }
                                }
                            }
                        }
                        catch (Exception E)
                        {}
                    }
                }
            }
            catch (Exception)
            {}
        }
    }
}