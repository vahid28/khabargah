﻿using KhabarGah.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KhabarGah.BLL.Rout
{
    public partial class Post : System.Web.UI.Page
    {
        public string result = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string post = Page.RouteData.Values["first"].ToString().Replace('-', ' ').Replace('=', '/').Replace('_', ':').Replace('.', '+');
            string second = Page.RouteData.Values["second"].ToString().Replace('-', ' ');
            string first = Page.RouteData.Values["post"].ToString().Replace('-', ' ');
            NewsDAL news = new NewsDAL();
            result = news.Writepost(first, second, post);
        }
    }
}