﻿using KhabarGah.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KhabarGah.BLL.Rout
{
    public partial class firstRout : System.Web.UI.Page
    {
        public string myResult;
        protected void Page_Load(object sender, EventArgs e)
        {
            string routWord = Page.RouteData.Values["first"].ToString().Replace('-', ' ');
            NewsDAL v = new NewsDAL();
            myResult = v.CustomhandleAgency_Category(routWord);
        }
    }
}