﻿using KhabarGah.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KhabarGah.BLL.Rout
{
    public partial class secondRout : System.Web.UI.Page
    {
        public string myResult;
        protected void Page_Load(object sender, EventArgs e)
        {
            string first = Page.RouteData.Values["first"].ToString().Replace('-', ' ');
            string second = Page.RouteData.Values["second"].ToString().Replace('-', ' ');
            NewsDAL news = new NewsDAL();
            myResult = news.WriteNews(first, second);

        }
    }
}