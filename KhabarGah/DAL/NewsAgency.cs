//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KhabarGah.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class NewsAgency
    {
        public NewsAgency()
        {
            this.News = new HashSet<News>();
            this.NewsCategory_AgencyRelation = new HashSet<NewsCategory_AgencyRelation>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<NewsCategory_AgencyRelation> NewsCategory_AgencyRelation { get; set; }
    }
}
