﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using KhabarGah.SignalR;
using Microsoft.AspNet.SignalR;
using HtmlAgilityPack;

namespace KhabarGah.DAL
{
    public class NewsDAL
    {
        //Three overload for add category, newsBody, NewsAgencieys
        KhabarGahEntities1 Model = new KhabarGahEntities1();
        
        private int NewsAgency;
        private int NewsCategory;
        public NewsDAL()
        {
            var a = 10;
            a++;
            if(!Model.Database.Exists())
            Model.Database.Create();
        }
        public NewsDAL(string NewsAgencyString, string NewsCategoryString)
        {
            if (!Model.NewsAgencies.Any(u => u.Name == NewsAgencyString))
            {
                NewsAgency newsAgencysInstance = new DAL.NewsAgency();
                newsAgencysInstance.Name = NewsAgencyString;
                Model.NewsAgencies.Add(newsAgencysInstance);
                Model.SaveChanges();
                NewsAgency = newsAgencysInstance.Id;
            }
            else
            {
                var a = from myNewsAgencyId in Model.NewsAgencies where myNewsAgencyId.Name == NewsAgencyString select myNewsAgencyId.Id;
                NewsAgency = a.SingleOrDefault();
            }
            if (!Model.NewsCategories.Any(u => u.Name == NewsCategoryString))
            {
                NewsCategory newsCategorysInstance = new DAL.NewsCategory();
                newsCategorysInstance.Name = NewsCategoryString;
                Model.NewsCategories.Add(newsCategorysInstance);
                try
                {
                    Model.SaveChanges();
                }
                catch (Exception)
                { 
                   
                }
                NewsCategory = newsCategorysInstance.Id;
            }
            else
            {
                var a = from myNewsCategoryId in Model.NewsCategories where myNewsCategoryId.Name == NewsCategoryString select myNewsCategoryId.Id;
                NewsCategory = a.SingleOrDefault();
            }
            if (!Model.NewsCategory_AgencyRelation.Any(u => u.NewsAgencyId == NewsAgency && u.NewsCategoryId == NewsCategory))
            {
                NewsCategory_AgencyRelation myRelation = new NewsCategory_AgencyRelation();
                myRelation.NewsAgencyId = NewsAgency;
                myRelation.NewsCategoryId = NewsCategory;
                Model.NewsCategory_AgencyRelation.Add(myRelation);
                Model.SaveChanges();
            }
        }
        public bool AddNews(string Title, string Html, string SourceLinke, DateTime Date)
        {
            if (!Model.News.Any(u => u.Title == Title && u.SourceLinke == SourceLinke))
            {
                News NewsInstance = new News();
                NewsInstance.AlertCount = 0;
                NewsInstance.ArchiveNumber = 0;
                NewsInstance.Date = Date;
                NewsInstance.Html = Html.Trim();
                NewsInstance.Id = Guid.NewGuid();
                NewsEmotion NewsEmotionInstance = new NewsEmotion();
                NewsEmotionInstance.Id = Guid.NewGuid();
                NewsEmotionInstance.SadNumber = 0;
                NewsEmotionInstance.HappyNumber = 0;
                Model.NewsEmotions.Add(NewsEmotionInstance);
                NewsInstance.NewsAgencyId = NewsAgency;
                NewsInstance.NewsEmotionId = NewsEmotionInstance.Id;
                NewsInstance.NewsCategoryId = NewsCategory;
                NewsInstance.ShareCount = 0;
                NewsInstance.SourceLinke = SourceLinke.Trim();
                NewsInstance.VisitsCount = 0;
                NewsInstance.Title = Title.Trim();
                Model.News.Add(NewsInstance);
                Model.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public void AddNews(string Title, string Html, string SourceLinke, DateTime Date, string publishDate,string description,string thubnail)
        {
            News NewsInstance = new News();
            NewsInstance.AlertCount = 0;
            NewsInstance.ArchiveNumber = 0;
            NewsInstance.Date = Date;
            NewsInstance.publishDateTime = publishDate;
            NewsInstance.Html = Html.Trim();
            NewsInstance.Id = Guid.NewGuid();
            NewsEmotion NewsEmotionInstance = new NewsEmotion();
            NewsEmotionInstance.Id = Guid.NewGuid();
            NewsEmotionInstance.SadNumber = 0;
            NewsEmotionInstance.HappyNumber = 0;
            Model.NewsEmotions.Add(NewsEmotionInstance);
            NewsInstance.NewsAgencyId = NewsAgency;
            NewsInstance.NewsEmotionId = NewsEmotionInstance.Id;
            NewsInstance.NewsCategoryId = NewsCategory;
            NewsInstance.ShareCount = 0;
            NewsInstance.SourceLinke = SourceLinke.Trim();
            NewsInstance.Description = description.Trim();
            NewsInstance.VisitsCount = 0;
            NewsInstance.Title = Title.Trim();
            NewsInstance.thumbnail = thubnail.Trim();
            Model.News.Add(NewsInstance);
            Model.SaveChanges();
            manageLastNews(NewsAgency, NewsCategory, description.Trim(), Html , Title.Trim(), thubnail.Trim());
        }

        private void manageLastNews(int NAI, int NCI, string Description, string html , string Title, string thumbnail)
        {
            var NCN = NewCategoryName(NCI);
            var NAN = NewAgencyName(NAI);
            StringBuilder myLastNews = new StringBuilder();
            myLastNews.Append("<div class='NewsBlocks'>");
            myLastNews.Append("<div class='right-NewsBlocks'>");
            myLastNews.Append("<a href ='#' ><img src='" + thumbnail+"' class='lastNewsImage' /></a>");
            myLastNews.Append("</div>");
            myLastNews.Append("<div class='left-NewsBlocks'>");
            myLastNews.Append("<a href='#' class='left-NewsBlocks-tile'>");
            myLastNews.Append(Title);
            myLastNews.Append("</a>");
            //myLastNews.Append("<p class='left-NewsBlocks-description'>");
            //if (Description != "")
            //{
            //    myLastNews.Append(Description.Take(100));
            //}
            //else
            //{
            //    myLastNews.Append(html.Take(100));
            //}
            //myLastNews.Append("</p>");
            myLastNews.Append("<div class='NewsFeatures'>");
            myLastNews.Append("<a href='#' class='left-NewsBlocks-NewsAgency'>");
            myLastNews.Append(NAN);
            myLastNews.Append("</a>");
            myLastNews.Append("،<a href='#' class='left-NewsBlocks-NewsCategory'>");
            myLastNews.Append(NCN);
            myLastNews.Append("</a>");
            myLastNews.Append("</div>");
            myLastNews.Append("</div>");
            myLastNews.Append("</div>");
            var context = GlobalHost.ConnectionManager.GetHubContext<MyNewsHub>();
            context.Clients.All.broadcastMessage(myLastNews.ToString());
        }
        public string gettingNews()
        {   
                StringBuilder myLastNews = new StringBuilder();
                var a = (from mycinstance in Model.News orderby mycinstance.Date descending select mycinstance).Take(4);
                foreach(var item in a)
                {
                    myLastNews.Append("<div class='NewsBlocks'>");
                    if (item.thumbnail != "")
                    {
                        myLastNews.Append("<div class='right-NewsBlocks'>");
                        myLastNews.Append("<a href ='#' ><img src='" + item.thumbnail + "' class='lastNewsImage' /></a>");
                        myLastNews.Append("</div>");
                    }
                    myLastNews.Append("<div class='left-NewsBlocks'>");
                    myLastNews.Append("<a href='#' class='left-NewsBlocks-tile'>");
                    myLastNews.Append(item.Title);
                    myLastNews.Append("</a>");
                    //myLastNews.Append("<p class='left-NewsBlocks-description'>");
                    //if (Description != "")
                    //{
                    //    myLastNews.Append(Description.Take(100));
                    //}
                    //else
                    //{
                    //    myLastNews.Append(html.Take(100));
                    //}
                    //myLastNews.Append("</p>");
                    myLastNews.Append("<div class='NewsFeatures'>");
                    myLastNews.Append("<a href='#' class='left-NewsBlocks-NewsAgency'>");
                    var t = from mycinstance in Model.NewsAgencies where mycinstance.Id == item.NewsAgencyId select mycinstance.Name;
                    myLastNews.Append(t.FirstOrDefault());
                    myLastNews.Append("</a>");
                    myLastNews.Append("،<a href='#' class='left-NewsBlocks-NewsCategory'>");
                    t = from mycinstance in Model.NewsCategories where mycinstance.Id == item.NewsCategoryId select mycinstance.Name;
                    myLastNews.Append(t.FirstOrDefault());
                    myLastNews.Append("</a>");
                    myLastNews.Append("</div>");
                    myLastNews.Append("</div>");
                    myLastNews.Append("</div>");
                }
                return myLastNews.ToString();
        }
        public string NewCategoryName(int NewsCategoryId)
        {
            var a = from myNewsCategoryName in Model.NewsCategories where myNewsCategoryName.Id == NewsCategoryId select myNewsCategoryName.Name;
            return a.SingleOrDefault();
        }
        public string NewAgencyName(int NewsAgencyId)
        {
            var a = from myNewsAgencyName in Model.NewsAgencies where myNewsAgencyName.Id == NewsAgencyId select myNewsAgencyName.Name;
            return a.SingleOrDefault();
        }
        public int NewCategoryId(string NewsCategoryName)
        {
            var a = from myNewsCategoryId in Model.NewsCategories where myNewsCategoryId.Name == NewsCategoryName select myNewsCategoryId.Id;
            return a.SingleOrDefault();
        }
        public Guid IdOfNews(string title)
        {
            var a = from myNews in Model.News where myNews.Title == title select myNews.Id;
            return a.SingleOrDefault();
        }
        public void setPublish(News b)
        {
            var a = from myInstance in Model.News where myInstance.Id == b.Id select myInstance;
            a.SingleOrDefault().publish = 1;
            Model.SaveChanges();
        }
        public bool checkNews(string title, string linke)
        {
            
            if (Model.News.Any(u => u.Title == title && u.SourceLinke == linke))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public string[] getAllNewsAgencies()
        {
            var a = from mycinstance in Model.NewsAgencies select mycinstance.Name;
            var b = (from mycinstance1 in Model.News orderby mycinstance1.Date descending select mycinstance1).Take(10);
            return a.ToArray();
        }
        public string[] getAllNewsCategories()
        {
            var a = from mycinstance in Model.NewsCategories select mycinstance.Name;
            return a.ToArray();
        }
        public string CustomhandleAgency_Category(string word)
        {
            if (Model.NewsCategories.Any(u => u.Name == word))
            {
                var a = from myCategory in Model.NewsCategories where myCategory.Name == word select myCategory.Id;
                var t = a.FirstOrDefault();
                var b = from myAgencies in Model.NewsCategory_AgencyRelation where myAgencies.NewsCategoryId == t select myAgencies.NewsAgencyId;
                LinkedList<string> NewsCategoriesNames = new LinkedList<string>();
                foreach (var myVar in b)
                {
                    var i = from myCTG in Model.NewsAgencies where myCTG.Id == myVar select myCTG.Name;
                    var ght = i.FirstOrDefault();
                    NewsCategoriesNames.AddLast(ght);
                }
                StringBuilder returnString = new StringBuilder();
                foreach (var myAttr in NewsCategoriesNames)
                {
                    returnString.Append("<p>");
                    returnString.Append("<a href='/" + myAttr.Replace(' ', '-') + "/" + word + "'>");
                    returnString.Append(myAttr);
                    returnString.Append("</a></p>");
                }
                return returnString.ToString();
            }
            else if (Model.NewsAgencies.Any(u => u.Name == word))
            {
                var a = from myAgencies in Model.NewsAgencies where myAgencies.Name == word select myAgencies.Id;
                var t = a.FirstOrDefault();
                var b = from myAgencies in Model.NewsCategory_AgencyRelation where myAgencies.NewsAgencyId == t select myAgencies.NewsCategoryId;
                LinkedList<string> NewsCategoriesNames = new LinkedList<string>();
                foreach (var myVar in b)
                {
                    var i = from myCTG in Model.NewsCategories where myCTG.Id == myVar select myCTG.Name;
                    var ght = i.FirstOrDefault();
                    NewsCategoriesNames.AddLast(ght);
                }

                StringBuilder returnString = new StringBuilder();
                foreach (var myAttr in NewsCategoriesNames)
                {
                    returnString.Append("<p>");
                    returnString.Append("<a href='/" + myAttr.Replace(' ', '-') + "/" + word.Replace(' ', '-') + "'>");
                    returnString.Append(myAttr);
                    returnString.Append("</a></p>");
                }
                return returnString.ToString();
            }
            else //write page that not exist
            {
                HttpContext.Current.Response.Status = "404 Not Found";
                HttpContext.Current.Response.StatusCode = 404;
                HttpContext.Current.Response.End();
            }
            return null;
        }
        public string WriteNews(string first, string second)
        {

            LinkedList<string> news = new LinkedList<string>();
            if (Model.NewsCategories.Any(u => u.Name == first)) //if first equal to a category name
            {
                var CategoryId = from u in Model.NewsCategories where u.Name == first select u.Id;
                var CtgId = CategoryId.FirstOrDefault();
                var AgencyId = from u in Model.NewsAgencies where u.Name == second select u.Id;
                var AgnId = AgencyId.FirstOrDefault();
                //now we have both of agency id and category id
                foreach (var u in AgencyId)
                {
                    var select = from a in Model.News where (a.NewsCategoryId == CtgId && a.NewsAgencyId == AgnId) select a.Title;
                    foreach (var a in select)
                        news.AddLast(a);
                }
            }
            else if (Model.NewsAgencies.Any(u => u.Name == first)) //if first equal to a agency name
            {
                var AgencyId = from u in Model.NewsAgencies where u.Name == first select u.Id;
                var AgnId = AgencyId.FirstOrDefault();
                var CategoryId = from u in Model.NewsCategories where u.Name == second select u.Id;
                var CtgId = CategoryId.FirstOrDefault();
                //now we have both of category id and agency id
                foreach (var u in CategoryId)
                {
                    var select = from a in Model.News where (a.NewsAgencyId == AgnId && a.NewsCategoryId == CtgId) select a.Title;
                    foreach (var a in select)
                        news.AddLast(a);
                }
            }
            StringBuilder result = new StringBuilder();
            foreach (var a in news)
            {
                result.Append("<p><a href='/" + a.Replace(' ', '-').Replace('/', '=').Replace(':', '_').Replace('+', '.') + "/" + first.Replace(' ', '-') + "/" + second.Replace(' ', '-') + "'>");
                result.Append(a);
                result.Append("</a></p>");
            }
            return result.ToString();
        }
        public string Writepost(string first, string second, string PostTitle)
        {
            string AgencyName = "";
            if (Model.NewsAgencies.Any(u => u.Name == first))
                AgencyName = (from u in Model.NewsAgencies where u.Name == first select u.Name).FirstOrDefault();
            else if (Model.NewsCategories.Any(u => u.Name == first))
                AgencyName = (from u in Model.NewsAgencies where u.Name == second select u.Name).FirstOrDefault();
            var a = (from myPost in Model.News where myPost.Title == PostTitle select myPost).First();
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(a.Html);
            string Content = string.Empty;
            switch (AgencyName)
            {
                case "خبرآنلاین":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='newsBodyCont']"))
                        Content = selectNode.InnerText;
                    break;
                case "الف":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@id='doc_div33']"))
                        Content = selectNode.InnerText;
                    break;
                case "تابناک":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='body']"))
                        Content = selectNode.InnerText;
                    break;
                case "ایرنا":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='BodyText']"))
                        Content = selectNode.InnerText;
                    break;
                case "دولت":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//span[@class='flR alJ lnH20  f12']"))
                        Content = selectNode.InnerText;
                    break;
                case "مشرق":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='body']"))
                        Content = selectNode.InnerText;
                    break;
                case "باشگاه خبرنگاران":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='body']"))
                        Content = selectNode.InnerText;
                    break;
                case "مهر":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='newsDetail BodyHolder alJ drR']"))
                        Content = selectNode.InnerText;
                    break;
                case "افکارنیوز":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//span[@id='content_span']"))
                        Content = selectNode.InnerText;
                    break;
                case "تسنیم":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='itemFullText']"))
                        Content = selectNode.InnerText;
                    break;
                case "فارس":
                    foreach (HtmlNode selectNode in htmlDocument.DocumentNode.SelectNodes("//div[@class='nwstxtmainpane']"))
                        Content = selectNode.InnerText;
                    break;
            }


            StringBuilder postCreator = new StringBuilder();
            postCreator.Append("<p>" + Content + "<br>");
            postCreator.Append("<br><p>Alert Count:" + a.AlertCount + "<br>Archive Number:" + a.ArchiveNumber + "<br>Date:" + a.Date + "<br>Describtion:" + a.Description + "<br>NewsAgency:" + a.NewsAgency + "<br>News Category:" + a.NewsCategory + "<br>News Emotion:" + a.NewsEmotion + "<br>PublishDateTime:" + a.publishDateTime + "<br>ShareCount:" + a.ShareCount + "<br>Picture:<img src=" + a.thumbnail + " />" + "<br>Visits Count:" + a.VisitsCount);
            postCreator.Append("</p>");
            postCreator.Append("</p>");
            return postCreator.ToString();

        }
    }
}