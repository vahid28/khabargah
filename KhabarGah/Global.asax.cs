﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace KhabarGah
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.MapPageRoute("FirstRoute",
            "{first}",
            "~/BLL/Rout/firstRout.aspx");
            RouteTable.Routes.MapPageRoute("SecondRoute",
                "{first}/{second}", "~/BLL/Rout/secondRout.aspx");
            RouteTable.Routes.MapPageRoute("PostRout",
    "{first}/{second}/{post}", "~/BLL/Rout/Post.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}