﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace KhabarGah.SignalR
{
    public class MyNewsHub : Hub
    {
        public void vahid(string title)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(title);
        }
    }
}